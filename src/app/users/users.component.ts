import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpEventType, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  private getUserUrls = 'http://localhost:63956/api/v1/Users/GetUsers';

  constructor(
    private http: HttpClient
  ) { }


  ngOnInit() {
    this.http
      .get(this.getUserUrls)
      .subscribe( (responce: Response) => {
            console.log(responce);

  });
}

}
